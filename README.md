**Detection of precursors of combustion instability using convolutional recurrent neural networks**

**Combustion and Flame** (CNF111558)

Author : Antony Cellier

Contact : cellier@cerfacs.fr

# INSTALLATION PROCEDURE

How to use the notebooks:

1. Create a Master folder
2. Clone repository in Master
3. Create a python 3.7 virtual environment and inside it:

```
# if you run without a gpu
pip install tensorflow==1.15.5
# if you run with a gpu (it may require a specific package such as cuda for nvidia)
pip install tensorflow-gpu==1.15.5

pip install keras==2.2.4
pip install matplotlib
```

4. Link the environment to jupyter:

```
ipython kernel install --user --name=path/to/your/virtual_env
```

5. When opening the notebook, select the right environment as the kernel.
6. Data (3.51 Go) can be downloaded at :  https://cerfacs.fr/opendata/documents/precursors-instability_CNF111558/DATA.tar.gz

**Your working environment should look like this:**

    - Master/
        - DATA/
            - inputs_targets/INPUTS_TARGETS...
            - signal_rolling_window/SOME_SIGNAL...
        - precursors_instability/
            - notebooks...
        - your_virtual_env/
            - your environment may appear here if you built it in Master/



