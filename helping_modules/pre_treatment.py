from scipy import *
from pylab import *
from cmath import *
import numpy as np

import keras
from keras.utils import np_utils

#Reshape the data to have a channel-last format
def reshape_channel_last(X):

    X_t = np.zeros((X.shape[0], X.shape[2], X.shape[1]))

    for i in range(X.shape[0]):
    
        X_t[i,:,:] = np.transpose(X[i,:,:])
    
    return X_t

#Transform type to the integer format
def to_int_1(X):

    for i in range(X.shape[0]):
        X[i] = int(X[i])
    
    return X

def to_int_2(X):
    
    Y = np.zeros((X.shape[0],1))
    for i in range(Y.shape[0]):
        Y[i,:] = int(X[i])
    
    return Y