import numpy as np
from sklearn.metrics import confusion_matrix, roc_curve, roc_auc_score
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt


# Computation of the accuracy based on a prediction array
# Compared to the target
def Acc_Calc(Y1, Y2):
    
    Acc_arr_det = np.zeros(Y1.shape)
    Acc_arr = np.zeros(Y1.shape[0])
    for k in range(Y1.shape[0]):
        for i in range(Y1.shape[1]):
            if Y1[k,i] != Y2[k,i]:
                Acc_arr_det[k,i] = 0
            else:
                Acc_arr_det[k,i] = 1
        
        Acc_arr[k] = np.mean(Acc_arr_det[k,:])    
            
    Acc = np.mean(Acc_arr)
    
    return(Acc)

# Computation of the binary cross entropy based on numpy log function
def BinC_Calc(Y1, Y2):
    
    BinC_arr_det = np.zeros(Y1.shape)
    BinC_arr = np.zeros(Y1.shape[0])
    for k in range(Y1.shape[0]):
        for i in range(Y1.shape[1]):
            if Y2[k,i] == 1:
                BinC_arr_det[k,i] = -np.log(Y1[k,i])
            else:
                BinC_arr_det[k,i] = -np.log(1-Y1[k,i])
            
        BinC_arr[k] = np.mean(BinC_arr_det[k,:])    
            
    BinC = np.mean(BinC_arr)
    
    return(BinC)

# Computation of the binary cross entropy as it is done in Keras
def Loss_Calc(Y1, Y2):
    
    Loss_arr_det = np.zeros(Y1.shape)
    Loss_arr = np.zeros(Y1.shape[0])
    for k in range(Y1.shape[0]):
        for i in range(Y1.shape[1]):
            
            P = np.clip(Y1[k,i], 1e-7, 0.9999999)
            Z = np.log(P / (1-P))
            
            Loss_arr_det[k,i] = max(Z,0) - Z*Y2[k,i] + np.log(1+np.exp(-np.abs(Z)))
            
        Loss_arr[k] = np.mean(Loss_arr_det[k,:])    
            
    Loss = np.mean(Loss_arr)
    
    return(Loss)

# Produce ROC curves 
def ROC(Targets, Pred_proba, Legend):
    
    N = Targets.shape[0]
    plot_marker = ['-k', '^-k', '*-k', 'o-k', '+-k']
    
    fpr_t = []
    tpr_t = []
    thr_t = []
    roc_auc_t = []
    
    for i in range(N):
        fpr_l = []
        tpr_l = []
        thr_l = []
        roc_auc_l = []
        for k in range(4):
            fpr, tpr, thr = roc_curve(Targets[i,:,k], Pred_proba[i,:,k])
            roc_auc = roc_auc_score(Targets[i,:,k], Pred_proba[i,:,k])
        
            fpr_l += [fpr]
            tpr_l += [tpr]
            thr_l += [thr]
            roc_auc_l += ['AUC = ' + str(round(roc_auc,2))]
        
        fpr_t += [fpr_l]
        tpr_t += [tpr_l]
        thr_t += [thr_l]
        roc_auc_t += [roc_auc_l]
    
    fig = plt.figure(constrained_layout=True, figsize=(12, 12))
    gs = gridspec.GridSpec(2, 2, figure=fig)

    #UP
    ax1 = fig.add_subplot(gs[0, 0])
    ax1.plot(fpr_t[0][0], tpr_t[0][0], plot_marker[0], LineWidth=4, label=Legend[0]+'\n'+roc_auc_t[0][0])
    ax1.fill_between(fpr_t[0][0], tpr_t[0][0], color='0.95')
    for i in range(1,N):
        ax1.plot(fpr_t[i][0], tpr_t[i][0], plot_marker[i], LineWidth=2, markevery=1, label=Legend[i]+'\n'+roc_auc_t[i][0])
        ax1.fill_between(fpr_t[i][0], tpr_t[i][0], color=str((0.95-0.05*i)))
    ax1.plot([-0.1,1.1], [-0.1,1.1], '--k')
    ax1.grid(axis='both')
    ax1.set_title('UP')
    ax1.set_xlim([-0.03, 1.03])
    ax1.set_ylim([-0.03, 1.03])
    ax1.set_xlabel('False Positive')
    ax1.set_ylabel('True Positive')
    ax1.legend(loc='lower right', fontsize=14)
    
    #RIGHT
    ax2 = fig.add_subplot(gs[0, 1])
    ax2.plot(fpr_t[0][1], tpr_t[0][1], 'k', LineWidth=4, label=Legend[0]+'\n'+roc_auc_t[0][1])
    ax2.fill_between(fpr_t[0][1], tpr_t[0][1], color='0.95')
    for i in range(1,N):
        ax2.plot(fpr_t[i][1], tpr_t[i][1], plot_marker[i], LineWidth=2, markevery=1, label=Legend[i]+'\n'+roc_auc_t[i][1])
        ax2.fill_between(fpr_t[i][1], tpr_t[i][1], color=str((0.95-0.05*i)))
    ax2.plot([-0.1,1.1], [-0.1,1.1], '--k')
    ax2.grid(axis='both')
    ax2.set_title('RIGHT')
    ax2.set_xlim([-0.03, 1.03])
    ax2.set_ylim([-0.03, 1.03])
    ax2.set_xlabel('False Positive')
    ax2.set_ylabel('True Positive')
    ax2.legend(loc='lower right', fontsize=14)
    
    #DOWN
    ax3 = fig.add_subplot(gs[1, 0])
    ax3.plot(fpr_t[0][2], tpr_t[0][2], 'k', LineWidth=4, label=Legend[0]+'\n'+roc_auc_t[0][2])
    ax3.fill_between(fpr_t[0][2], tpr_t[0][2], color='0.95')
    for i in range(1,N):
        ax3.plot(fpr_t[i][2], tpr_t[i][2], plot_marker[i], LineWidth=2, markevery=1, label=Legend[i]+'\n'+roc_auc_t[i][2])
        ax3.fill_between(fpr_t[i][2], tpr_t[i][2], color=str((0.95-0.05*i)))                
    ax3.plot([-0.1,1.1], [-0.1,1.1], '--k')
    ax3.grid(axis='both')
    ax3.set_title('DOWN')
    ax3.set_xlim([-0.03, 1.03])
    ax3.set_ylim([-0.03, 1.03])
    ax3.set_xlabel('False Positive')
    ax3.set_ylabel('True Positive')
    ax3.legend(loc='lower right', fontsize=14)
    
    #LEFT
    ax4 = fig.add_subplot(gs[1, 1])
    ax4.plot(fpr_t[0][3], tpr_t[0][3], 'k', LineWidth=4, label=Legend[0]+'\n'+roc_auc_t[0][3])
    ax4.fill_between(fpr_t[0][3], tpr_t[0][3], color='0.95')
    for i in range(1,N):
        ax4.plot(fpr_t[i][3], tpr_t[i][3], plot_marker[i], LineWidth=2, markevery=1, label=Legend[i]+'\n'+roc_auc_t[i][3])
        ax4.fill_between(fpr_t[i][3], tpr_t[i][3], color=str((0.95-0.05*i)))
    ax4.plot([-0.1,1.1], [-0.1,1.1], '--k')
    ax4.grid(axis='both')
    ax4.set_title('LEFT')
    ax4.set_xlim([-0.03, 1.03])
    ax4.set_ylim([-0.03, 1.03])
    ax4.set_xlabel('False Positive')
    ax4.set_ylabel('True Positive')
    ax4.legend(loc='lower right', fontsize=14)
    
    return

# Show a confusion matrix
def CONF(Targets, Pred):
    print(' ')
    print('UP')
    MV0 = confusion_matrix(Targets[:,0], Pred[:,0])
    print(np.round(100*MV0/np.sum(MV0),2))
    print(' ')
    print('RIGHT')
    MV1 = confusion_matrix(Targets[:,1], Pred[:,1])
    print(np.round(100*MV1/np.sum(MV1),2))
    print(' ')
    print('DOWN')
    MV2 = confusion_matrix(Targets[:,2], Pred[:,2])
    print(np.round(100*MV2/np.sum(MV2),2))
    print(' ')
    print('LEFT')
    MV3 = confusion_matrix(Targets[:,3], Pred[:,3])
    print(np.round(100*MV3/np.sum(MV3),2))
    
    return